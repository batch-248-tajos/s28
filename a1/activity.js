//step 3. insert single room
db.rooms.insert({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
})

//step 4. insert many room
db.rooms.insertMany([
	{
		name:"double",
		accomodates:3,
		price:2000,
		description:"A room fit for a small family going on a vacation",
		rooms_available:5,
		isAvailable:false
	},
	{
		name:"queen",
		accomodates:4,
		price:4000,
		description:"A room with a queen sized bed perfect for a simple getaway",
		rooms_available:15,
		isAvailable:false
	}
])

//step 5. Use the find method to search for a room with the name double.
db.rooms.find({name:"double"})

//step 6. Use the updateOne method to update the queen room and set the available rooms to 0.
db.rooms.updateOne(
	{name:"queen"},
	{
		$set: {
			rooms_available:0
		}
	}
)

//step 7. Use the deleteMany method rooms to delete all rooms that have 0 availability.
db.rooms.deleteMany({
  rooms_available:0  
})